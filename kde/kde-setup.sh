#!/bin/bash
#
# kde-setup.sh
#
# (c) Niki Kovacs 2022 <info@microlinux.fr>

# Current directory
cwd=$(pwd)

# Slow things down a bit
sleep=1

# Remove these packages
cruft=$(egrep -v '(^\#)|(^\s+$)' ${cwd}/cruft.txt)

# Log
log="/tmp/$(basename "${0}" .sh).log"
echo > ${log}

# Menus
entriesdir="${cwd}/menus"
entries=$(ls ${entriesdir})
menudirs="/usr/share/applications"

# Make sure the script is being executed with superuser privileges.
if [[ "${UID}" -ne 0 ]]
then
  echo
  echo "  Please run with sudo or as root." >&2
  echo
  exit 1
fi

# Make sure we're running Rocky Linux 8.x.
if [ -f /etc/os-release ]
then
  source /etc/os-release
  SYSTEM="${ROCKY_SUPPORT_PRODUCT}"
  VERSION="${ROCKY_SUPPORT_PRODUCT_VERSION}"
fi
if [ "${SYSTEM}" != "Rocky Linux" ] && [ "${VERSION}" != "8" ] 
then
  echo
  echo "  Unsupported operating system." >&2
  echo
  exit 1
fi

sleep ${sleep}
echo
echo "  #####################"
echo "  # Rocky Linux ${VERSION} KDE #"
echo "  #####################"
echo
sleep ${sleep}

# Defined users
users="$(awk -F: '$3 > 999 {print $1}' /etc/passwd | sort)"

# KDE
echo "  Installing KDE desktop environment."
dnf group install -y "KDE Plasma Workspaces" >> ${log} 2>&1
sleep ${sleep}

# Remove useless packages
echo "  Removing unneeded components from the system."
sleep ${sleep}
for package in ${cruft}
do
  if rpm -q ${package} > /dev/null 2>&1
  then
    echo "  Removing package: ${package}"
    dnf -y remove ${package} >> ${log} 2>&1
    if [ "${?}" -ne 0 ]
      then
      echo "  Could not remove package ${package}." >&2
      exit 1
    fi
  fi
done
echo "  Unneeded components removed from the system."
sleep ${sleep}

echo "  Installing custom desktop menu."
sleep ${sleep}
for menudir in ${menudirs}
do
  for entry in ${entries}
  do
    if [ -r ${menudir}/${entry} ]
    then
      echo "  Installing menu item: ${entry}"
      cat ${entriesdir}/${entry} > ${menudir}/${entry}
      sleep 0.2
    fi
  done
done
echo "  Updating desktop database."
update-desktop-database
sleep ${sleep}
echo "  Custom desktop menu installed."
sleep ${sleep}

echo

exit 0
