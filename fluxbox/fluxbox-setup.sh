#!/bin/bash
#
# fluxbox-setup.sh
#
# (c) Niki Kovacs 2022 <info@microlinux.fr>
#
# This script installs and configures the X Window System and the lightweight
# Fluxbox window manager on a base Rocky Linux 8.x system.

# Current directory
cwd=$(pwd)

# Slow things down a bit
sleep=1

# Log
log="/tmp/$(basename "${0}" .sh).log"
echo > ${log}

# Make sure the script is being executed with superuser privileges.
if [[ "${UID}" -ne 0 ]]
then
  echo
  echo "  Please run with sudo or as root." >&2
  echo
  exit 1
fi

# Make sure we're running Rocky Linux 8.x.
if [ -f /etc/os-release ]
then
  source /etc/os-release
  SYSTEM="${ROCKY_SUPPORT_PRODUCT}"
  VERSION="${ROCKY_SUPPORT_PRODUCT_VERSION}"
fi
if [ "${SYSTEM}" != "Rocky Linux" ] && [ "${VERSION}" != "8" ] 
then
  echo
  echo "  Unsupported operating system." >&2
  echo
  exit 1
fi

sleep ${sleep}
echo
echo "  #########################"
echo "  # Rocky Linux ${VERSION} Fluxbox #"
echo "  #########################"
echo
sleep ${sleep}

# Defined users
users="$(awk -F: '$3 > 999 {print $1}' /etc/passwd | sort)"

# X11
echo "  Installing X Window System."
dnf group install -y base-x > ${log} 2>&1
sleep ${sleep}

# Fonts
echo "  Installing basic fonts."
dnf group install -y fonts >> ${log} 2>&1
dnf install -y xorg-x11-fonts-* >> ${log} 2>&1
sleep ${sleep}

# Fluxbox
echo "  Installing Fluxbox window manager."
dnf install -y fluxbox >> ${log} 2>&1
sleep ${sleep}
echo "  Configuring Fluxbox for future users."
rm -rf /etc/skel/.fluxbox
sleep ${sleep}
tar -xzf ${cwd}/fluxbox-config.tar.gz -C /etc/skel
  if [ ! -z "${users}" ]
  then
    for user in ${users}
    do
      if [ -d /home/${user} ]
      then
        echo "  Configuring Fluxbox for user: ${user}"
        cp -R /etc/skel/.fluxbox /home/${user}/
        chown -R ${user}:${user} /home/${user}/.fluxbox
        sleep ${sleep}
      fi
    done
  fi

# Applications
echo "  Installing basic applications."
dnf install -y xterm firefox >> ${log} 2>&1
sleep ${sleep}

# Xterm
echo "  Configuring Xterm."
cat ${cwd}/Xresources > /etc/X11/Xresources
sleep ${sleep}

# SDDM
echo "  Installing login manager."
dnf install -y sddm >> ${log} 2>&1
sleep ${sleep}

echo

exit 0
